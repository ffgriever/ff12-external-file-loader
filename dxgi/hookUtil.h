#pragma once
#include <filesystem>
#include <string>

enum langEnum
{
	LANG_JP = 0,
	LANG_US,
	LANG_FR,
	LANG_DE,
	LANG_IT,
	LANG_ES,
	LANG_UK,
	LANG_KR,
	LANG_CH,
	LANG_ASIA,
	LANG_IN,
	LANG_CN
};

std::filesystem::path buildPath(const char* fileName);
std::string replaceAll(const std::string& source, const std::string& from, const std::string& to);
std::string replaceLanguage(const std::string& source, int language);
bool DirectoryExists(const std::string& path);