#include <filesystem>
#include <fstream>
#include <string>
#include <set>
#include <regex>

#include "hook.h"
#include "util.h"
#include "hookUtil.h"
#include <Windows.h>
#if 0
void __fastcall readVbfFileHook(char* fileName, char* dest, int startOffset, unsigned int maxSize, unsigned int* a5)
{
	try
	{
		std::filesystem::path filePath = std::filesystem::path("..\\mods") / std::filesystem::path(fileName);
		if (std::filesystem::exists(filePath))
		{
			std::ifstream dirFile(filePath, std::ios::binary | std::ios::ate);
			std::streamsize size = dirFile.tellg();
			dirFile.seekg(startOffset, std::ios::beg);

			std::streamsize readSize = maxSize > size ? size : maxSize;
			LOG(INFO) << "Reading DIR: " << fileName << " (" << startOffset << ", " << maxSize << ")";
			if (!dirFile.read(dest, readSize))
			{
				LOG(WARN) << "Reading standalone file has failed, falling back to VBF";
			}
			else
			{
				/*if (*(uint64_t*)0x1F81160ULL && a5)
				{
					*(uint64_t*)0x1F82CC0ULL = (uint64_t)a5;
					*a5 = readSize;
				}*/
				//*a5 = 0;
				return;
			}
		}
		else
		{
			LOG(INFO) << "Reading VBF: " << fileName;
		}
	}
	catch (...) {}
	readVbfFile(fileName, dest, startOffset, maxSize, a5);
}

void __fastcall readInternalHook(void* unkPointer, tVbfParam* param, char flag)
{
	try
	{
		std::filesystem::path filePath = std::filesystem::path("..\\mods") / std::filesystem::path(param->filename);
		if (std::filesystem::exists(filePath))
		{
			std::ifstream dirFile(filePath, std::ios::binary | std::ios::in);
			std::streamsize size = std::filesystem::file_size(filePath);
			dirFile.seekg(param->startOffset, std::ios::beg);

			std::streamsize readSize = param->maxSize > size ? size : param->maxSize;
			LOG(INFO) << "Reading DIR: " << param->filename << " (" << param->startOffset << ", " << param->maxSize << ") " << std::hex << (uint64_t)param->dest << std::dec;
			if (!dirFile.read(param->dest, readSize))
			{
				LOG(WARN) << "Reading standalone file has failed, falling back to VBF";
			}
			else
			{
				if (param->callbackOffset)
					param->callbackOffset(readSize);
				return;
			}
		}
		else
		{
			LOG(INFO) << "Reading VBF: " << param->filename;
		}
	}
	catch (...) {}
	readInternal(unkPointer, param, flag);
}

void __fastcall readOldHook(void* unkPointer, tVbfParam* param)
{
	try
	{
		if (param->fileType == 1 || param->fileType == 3)
		{
			LOG(INFO) << "Reading VBF: " << param->filename << " (" << param->fileType << ", " << +param->unknownBytes[0] << ")";
			readOld(unkPointer, param);
			return;
		}

		std::filesystem::path filePath = std::filesystem::path("..\\mods") / std::filesystem::path(param->filename);
		if (std::filesystem::exists(filePath))
		{
			std::streamsize size = std::filesystem::file_size(filePath);

			char* dest = nullptr;
			std::streamsize maxSize = 0;
			std::streamsize startOffset = 0;
			switch (param->fileType)
			{
				case 2: //ps2file
					dest = param->dest;
					maxSize = param->maxSize;
					startOffset = param->startOffset;
					break;
				case 4: //text file
					if (size > 0)
					{
						*(char**)param->dest = (char*)ff12Malloc(size + 1);
						if (param->startOffset)
							*(char**)param->startOffset = *(char**)param->dest + size;
					}
					else
					{
						*(char**)param->dest = nullptr;
					}
					dest = *(char**)param->dest;
					maxSize = size;
					startOffset = 0;
					break;
			}

			std::streamsize readSize = maxSize > size ? size : maxSize;
			if (size > 0)
			{

				std::ifstream dirFile(filePath, std::ios::binary | std::ios::in);
				dirFile.seekg(startOffset, std::ios::beg);

				LOG(INFO) << param->fileType << " Reading DIR: " << param->filename << " (" << startOffset << ", " << maxSize << ") " << std::hex << (uint64_t)dest << std::dec;
				if (!dirFile.read(dest, readSize))
				{
					LOG(WARN) << "Reading standalone file has failed, falling back to VBF";
					readOld(unkPointer, param);
					return;
				}

				if (param->fileType == 4) //add zero at the end of text file
				{
					if (dest)
						dest[size] = 0;
				}
			}
			else
			{
				LOG(WARN) << "Reading DIR: " << param->filename << " - file empty";
			}

			if (param->unknownBytes[0] && param->callbackOffset)
				param->callbackOffset(readSize);

			return;
		}
		else
		{
			LOG(INFO) << "Reading VBF: " << param->filename;
		}
	}
	catch (...) {}
	readOld(unkPointer, param);
}
#endif

//do not propagate exceptions to a code compiled with a different compiler
bool generalRead(const std::filesystem::path& filePath, char* dest, std::streamsize startOffset, std::streamsize maxSize)
{
	try
	{
		if (std::filesystem::exists(filePath))
		{
			std::streamsize size = std::filesystem::file_size(filePath);


			std::streamsize readSize = maxSize > size ? size : maxSize;
			if (size > 0)
			{

				std::ifstream dirFile(filePath, std::ios::binary | std::ios::in);
				dirFile.seekg(startOffset, std::ios::beg);

				if (!dirFile.read(dest, readSize))
				{
					return false;
				}
			}
			else
			{
				LOG(WARN) << "Reading DIR: " << filePath.c_str() << " - file empty";
			}

			return true;
		}
		return false;
	}
	catch (...)
	{
		return false;
	}
}

bool testHooks(const tHookInit* hooks, size_t count)
{
	for (size_t i = 0; i < count; i++)
	{
		if (memcmp((void*)hooks[i].callPtr, hooks[i].original, 5) != 0)
			return false;
	}
	return true;
}

bool initHooks(const tHookInit* hooks, size_t count)
{
	if (!testHooks(hooks, count))
	{
		LOG(INFO) << "Invalid game version. Unable to find proper hook values.";
		return false;
	}

	HANDLE hProcess = GetCurrentProcess();

	for (size_t i = 0; i < count; i++)
	{
		intptr_t hookAddr = intptr_t(hooks[i].hookPtr);
		int32_t callRelativeAddr = int32_t(hookAddr - (hooks[i].callPtr + 5));
		if (!WriteProcessMemory(hProcess, (LPVOID)(hooks[i].callPtr + 1), &callRelativeAddr, 4, NULL))
		{
			LOG(INFO) << "Unable to write " << hooks[i].name;
			return false;
		}
	}
	return true;
}

void __fastcall readVBFType1Hook(void* unkPointer, const char* filename, int flag)
{
	try
	{
		LOG(WARN) << "Reading VBF1: " << filename;
	}
	catch (...) {}
	readVBFType1(unkPointer, filename, flag);
}

void __fastcall readVBFType2Hook(void* unkPointer, tVbfParam* param)
{
	try
	{
		auto filePath = buildPath(param->filename);
		if (generalRead(filePath, param->dest, param->startOffset, param->maxSize))
		{
			LOG(INFO) << "Reading DIR2: " << param->filename << " (" << param->startOffset << ", " << param->maxSize << ") " << std::hex << (uint64_t)param->dest << std::dec;
			return;
		}
		LOG(INFO) << "Reading VBF2: " << param->filename;
	}
	catch (...) {}
	readVBFType2(unkPointer, param);
}

void __fastcall readVBFType3Hook(void* unkPointer, tVbfParam* param)
{
	try
	{
		/*auto filePath = buildPath(param->filename);
		if (generalRead(filePath, param->dest, param->startOffset, param->maxSize))
		{
			LOG(INFO) << "Reading DIR2: " << param->filename << " (" << param->startOffset << ", " << param->maxSize << ") " << std::hex << (uint64_t)param->dest << std::dec;
			return;
		}*/
		LOG(WARN) << "Reading VBF3: " << param->filename << "; not implemented, passing through";
	}
	catch (...) {}
	readVBFType3(unkPointer, param);
}

void __fastcall readVBFType4Hook(void* unkPointer, tVbfParam* param)
{
	try
	{
		auto filePath = buildPath(param->filename);

		std::streamsize size = std::filesystem::file_size(filePath);
		if (size > 0)
		{
			*(char**)param->dest = (char*)ff12Malloc(size + 1);

			if (*(char**)param->dest != nullptr)
			{
				if (param->startOffset)
					*(char**)param->startOffset = *(char**)param->dest + size;

				(*(char**)param->dest)[size] = 0;

				if (generalRead(filePath, *(char**)param->dest, 0, size))
				{
					LOG(INFO) << "Reading DIR4: " << param->filename << " (" << 0 << ", " << size << ") " << std::hex << (uint64_t) * (char**)param->dest << std::dec;
					return;
				}
				else
				{
					ff12Free(*(char**)param->dest);
				}
			}
		}
		else
		{
			*(char**)param->dest = nullptr;
		}
		LOG(INFO) << "Reading VBF4: " << param->filename;
	}
	catch (...) {
		if (*(char**)param->dest != nullptr)
			ff12Free(*(char**)param->dest);
	}
	readVBFType4(unkPointer, param);
}

int __fastcall loadFstHook(int lang, int a2)
{
	int retval = loadFst(lang, a2);

	try
	{
		std::string offsetName = "sizeOffsets.txt", dirName = buildPath("").string();

		if (!DirectoryExists(dirName))
			throw std::runtime_error("Directory \"" + dirName + " doesn't exist\"");

		std::fstream offsetIn(offsetName, std::ios::in);
		if (!offsetIn.good()) throw std::runtime_error("Cannot open input file \"" + offsetName + "\"");

		std::set<std::string> listSet;
		std::string fileLine;

		static const std::regex rxProperLine("^([^!].*?);\\s*(0x[0-9a-fA-F]+)\\s*$");
		while (std::getline(offsetIn, fileLine))
		{
			namespace fs = std::filesystem;

			std::smatch rxProperLineMatch;

			std::regex_match(fileLine, rxProperLineMatch, rxProperLine);

			if (rxProperLineMatch.size() == 3)
			{
				std::string fullPath;
				std::string foundPath = rxProperLineMatch[1].str();
				if (lang != LANG_US)
					foundPath = replaceLanguage(foundPath, lang);
				fullPath = dirName + "/" + foundPath;

				if (listSet.size() && (listSet.find(foundPath) == listSet.end()))
					continue;

				if (fs::is_regular_file(fullPath))
				{
					uint32_t newFilesize = static_cast<uint32_t>(fs::file_size(fullPath));
					try
					{
						uint32_t offset = std::stoul(rxProperLineMatch[2], nullptr, 16);

						if (offset <= fileSizeTableMax - 4) *(uint32_t*)&fileSizeTable[offset] = newFilesize;

						LOG(INFO) << "Patched \"" << foundPath << "\", new size: " << newFilesize;
					}
					catch (std::invalid_argument e)
					{
						LOG(ERR) << "Conversion impossible, skipping";
					}
					catch (std::out_of_range e)
					{
						LOG(ERR) << L"Converted number too big, skipping";
					}
				}
			}
		}
	}
	catch (std::runtime_error& e)
	{
		LOG(ERR) << "Couldn't patch file size table: " << e.what();
	}
	catch (...)
	{
		LOG(ERR) << "Couldn't patch file size table!";
	}

	return retval;
}
