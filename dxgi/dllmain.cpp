#include <windows.h>
#include "blacklist.h"
#include "dxgi.h"
#include "util.h"
#include "hook.h"

static bool blacklisted = false;


BOOL WINAPI DllMain(HINSTANCE hInst, DWORD reason, LPVOID) {
	if (reason == DLL_PROCESS_ATTACH) {
		TCHAR DllPath[MAX_PATH], szSystemPath[MAX_PATH];
		// Find and load the real dxgi.dll
		GetModuleFileName(hInst, DllPath, MAX_PATH);
		TCHAR *DllName = strrchr(DllPath, '\\');

		GetSystemDirectory(szSystemPath, MAX_PATH);
		strcat_s(szSystemPath, DllName);
		initDLL(szSystemPath);

		blacklisted = isBlackListed();
		if (!blacklisted) {
			LOG(INFO) << "Final Fantasy XII External File Loader v1.0 by ffgriever";
			LOG(INFO) << "Hijacking " << DllName;
			LOG(INFO) << "Initialized: " << szSystemPath;
			initHooks(allVbfHooks, _countof(allVbfHooks));
		}
	}

	if (reason == DLL_PROCESS_DETACH) {
		if (!blacklisted) {
			LOG(INFO) << "Cleaning up";
		}
		freeDLL();
	}
	return TRUE;
}
