#pragma once
#include <filesystem>
#include <array>

//just in case
using ff12MallocType = void* (__cdecl* )(size_t size);
const ff12MallocType ff12Malloc = (ff12MallocType)0x85E730;

using ff12FreeType = void (__cdecl*)(void* offset);
const ff12FreeType ff12Free = (ff12FreeType)0x19C590;

using readCallbackType = int64_t(__fastcall*)(int result);
struct tVbfParam
{
	uint32_t fileType;				//0
	uint32_t unknown2;				//4
	readCallbackType callbackOffset;//8
	uint64_t unknownZero1;			//16
	uint8_t unknownBytes[8];		//24
	char filename[256];				//32
	char* dest;						//288
	uint64_t startOffset;			//296
	int64_t maxSize;				//304
};

using readVbfFileType = void(__fastcall*)(char* fileName, char* dest, int startOffset, unsigned int maxSize, unsigned int* a5);
const readVbfFileType readVbfFile = (readVbfFileType)0x1D6D90;

using readInternalType = void(__fastcall*)(void* unkPointer, tVbfParam* param, char flag);
const readInternalType readInternal = (readInternalType)0x1A0BB0;

//read types
using readFinalType1 = void(__fastcall*)(void* unkPointer, const char* filename, int flag);
using readFinalType = void(__fastcall*)(void* unkPointer, tVbfParam* param);
const readFinalType readOld = (readFinalType)0x1A07B0;
const readFinalType1 readVBFType1 = (readFinalType1)0x19CB20;
const readFinalType readVBFType2 = (readFinalType)0x1A0CF0;
const readFinalType readVBFType3 = (readFinalType)0x1A0E50;
const readFinalType readVBFType4 = (readFinalType)0x1A0F80;

//file size table
uint8_t* const fileSizeTable = (uint8_t*)0x2EC3EE0ULL;
constexpr size_t fileSizeTableMax = 820000ULL;
using loadFstType = int(__fastcall*)(int lang, int a2);
const loadFstType loadFst = (loadFstType)0x2DF9A0;

//calls to hook
#define READVBF1 0x2DF777LL
#define READVBF2 0x2DF853LL
#define LOADFSTCALL 0x22A159LL

#define READ1 0x1D6E51

#define READOLD1 0x1A0BDE
#define READOLD2 0x1A0C7E
#define READOLD3 0x1A12E4

#define READTYPE1 0x1A0822
#define READTYPE2 0x1A080C
#define READTYPE3 0x1A07FF
#define READTYPE4 0x1A07F2

void __fastcall readVbfFileHook(char* fileName, char* dest, int startOffset, unsigned int maxSize, unsigned int* a5);
void __fastcall readInternalHook(void* unkPointer, tVbfParam* param, char flag);
void __fastcall readOldHook(void* unkPointer, tVbfParam* param);
void __fastcall readVBFType1Hook(void* unkPointer, const char* filename, int flag);
void __fastcall readVBFType2Hook(void* unkPointer, tVbfParam* param);
void __fastcall readVBFType3Hook(void* unkPointer, tVbfParam* param);
void __fastcall readVBFType4Hook(void* unkPointer, tVbfParam* param);
int __fastcall loadFstHook(int lang, int a2);

bool generalRead(const std::filesystem::path& filePath, char* dest, std::streamsize startOffset, std::streamsize maxSize);

struct tHookInit
{
	const char* name;
	intptr_t callPtr;
	intptr_t hookPtr;
	const uint8_t original[5];
};

static const tHookInit allVbfHooks[] = {
	{"hookFST", LOADFSTCALL, (intptr_t)&loadFstHook, {0xE8, 0x42, 0x58, 0x0B, 0x00}},
	{"hookType1", READTYPE1, (intptr_t)&readVBFType1Hook, {0xE8, 0xF9, 0xC2, 0xFF, 0xFF}},
	{"hookType2", READTYPE2, (intptr_t)&readVBFType2Hook, {0xE8, 0xDF, 0x04, 0x00, 0x00}},
	{"hookType3", READTYPE3, (intptr_t)&readVBFType3Hook, {0xE8, 0x4C, 0x06, 0x00, 0x00}},
	{"hookType4", READTYPE4, (intptr_t)&readVBFType4Hook, {0xE8, 0x89, 0x07, 0x00, 0x00}},
	//{"hookOld1", READOLD1, (intptr_t)&readOldHook, {0, 0, 0, 0, 0}},
	//{"hookOld2", READOLD2, (intptr_t)&readOldHook, {0, 0, 0, 0, 0}},
	//{"hookOld3", READOLD3, (intptr_t)&readOldHook, {0, 0, 0, 0, 0}},
};

bool testHooks(const tHookInit* hooks, size_t count);
bool initHooks(const tHookInit* hooks, size_t count);
