#include "hookUtil.h"

std::string replaceLanguage(const std::string& source, int language)
{
	if (language == LANG_US)
		return source;

	std::string replacement;
	switch (language)
	{
		case LANG_DE:
			replacement = "/de/";
			break;
		case LANG_ES:
			replacement = "/es/";
			break;
		case LANG_FR:
			replacement = "/fr/";
			break;
		case LANG_IT:
			replacement = "/it/";
			break;
		case LANG_JP:
		case LANG_IN:
			replacement = "/in/";
			break;
		case LANG_KR:
			replacement = "/kr/";
			break;
		case LANG_CH:
			replacement = "/ch/";
			break;
		case LANG_CN:
			replacement = "/cn/";
			break;
		default:
			replacement = "/us/";
			break;
	}
	return replaceAll(source, "/us/", replacement);
}

bool DirectoryExists(const std::string& path)
{
	struct _stat info;
	if (_stat(path.c_str(), &info) != 0)
		return false;
	else if (info.st_mode & S_IFDIR)
		return true;

	return false;
}

std::filesystem::path buildPath(const char* fileName)
{
	std::filesystem::path filePath = std::filesystem::path("..\\mods\\unpacked") / std::filesystem::path(fileName);
	return filePath;
}

std::string replaceAll(const std::string& source, const std::string& from, const std::string& to)
{
	std::string newString;
	newString.reserve(source.length());  // avoids a few memory allocations

	std::string::size_type lastPos = 0;
	std::string::size_type findPos;

	while (std::string::npos != (findPos = source.find(from, lastPos)))
	{
		newString.append(source, lastPos, findPos - lastPos);
		newString += to;
		lastPos = findPos + from.length();
	}

	// Care for the rest after last occurrence
	newString += source.substr(lastPos);

	return newString;
}
