# Caution

This is an outdated version of the loader. A new one, with a completely different approach is available [here](https://gitlab.com/ffgriever/ff12-external-file-loader-module).
